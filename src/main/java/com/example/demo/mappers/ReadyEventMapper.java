package com.example.demo.mappers;

import com.example.demo.dto.ReadyEventDTO;
import com.example.demo.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ReadyEventMapper {

    ReadyEventMapper INSTANCE = Mappers.getMapper(ReadyEventMapper.class);

    ReadyEventDTO toDTO(Event event);

    Event toEntity(ReadyEventDTO readyEventDTO);
}
