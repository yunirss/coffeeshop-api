package com.example.demo.mappers;

import com.example.demo.dto.RegisterEventDTO;
import com.example.demo.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RegisterEventMapper {

    RegisterEventMapper INSTANCE = Mappers.getMapper(RegisterEventMapper.class);

    RegisterEventDTO toDTO(Event event);

    Event toEntity(RegisterEventDTO registerEventDTO);
}
