package com.example.demo.mappers;

import com.example.demo.dto.InProgressEventDTO;
import com.example.demo.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InProgressEventMapper {

    InProgressEventMapper INSTANCE = Mappers.getMapper(InProgressEventMapper.class);

    InProgressEventDTO toDTO(Event event);

    Event toEntity(InProgressEventDTO inProgressEventDTO);
}
