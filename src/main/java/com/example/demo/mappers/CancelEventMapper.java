package com.example.demo.mappers;

import com.example.demo.dto.CancelEventDTO;
import com.example.demo.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CancelEventMapper {

    CancelEventMapper INSTANCE = Mappers.getMapper(CancelEventMapper.class);

    CancelEventDTO toDTO(Event event);

    Event toEntity(CancelEventDTO cancelEventDTO);
}
