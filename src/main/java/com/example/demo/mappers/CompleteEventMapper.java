package com.example.demo.mappers;

import com.example.demo.dto.CompleteEventDTO;
import com.example.demo.entity.Event;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompleteEventMapper {

    CompleteEventMapper INSTANCE = Mappers.getMapper(CompleteEventMapper.class);

    CompleteEventDTO toDTO(Event event);

    Event toEntity(CompleteEventDTO completeEventDTO);
}
