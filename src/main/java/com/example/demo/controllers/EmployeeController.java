package com.example.demo.controllers;

import com.example.demo.entity.Employee;
import com.example.demo.exceptions.NoSuchEntityException;
import com.example.demo.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/employees")
public class EmployeeController {
    private final EmployeeService service;

    public EmployeeController(EmployeeService employeeService) {
        this.service = employeeService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> save(@RequestBody Employee employee) {
        service.save(employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> update(@RequestBody Employee employee) {
        service.save(employee);
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> findAll() throws NoSuchEntityException {
        List<Employee> employees = service.findAll();
        if (employees.isEmpty()) {
            throw new NoSuchEntityException("There is no employees");
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> findEmployeeById(@PathVariable Integer id) throws NoSuchEntityException {
        Employee employee = service.findById(id);
        if (employee == null) {
            throw new NoSuchEntityException("There is no employee with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) {
        service.deleteById(id);
        return "Employee with Id = " + id + " was deleted";
    }
}
