package com.example.demo.controllers;

import com.example.demo.dto.*;
import com.example.demo.entity.Event;
import com.example.demo.mappers.*;
import com.example.demo.service.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {
    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Event>> findAll() {
        List<Event> events = eventService.findAll();
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @GetMapping(value = "/getEventsByOrderId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Event>> findEventsByOrderId(@PathVariable Integer id) {
        List<Event> events = eventService.findEventsByOrderId(id);
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> registerEvent(@RequestBody RegisterEventDTO eventDTO) {
        Event event = RegisterEventMapper.INSTANCE.toEntity(eventDTO);
        eventService.publishEvent(event);
        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @PostMapping(value = "/cancel", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> cancelOrder(@RequestBody CancelEventDTO eventDTO) {
        Event event = CancelEventMapper.INSTANCE.toEntity(eventDTO);
        eventService.publishEvent(event);
        return new ResponseEntity<>(event,HttpStatus.OK);
    }

    @PostMapping(value = "/ready", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> readyEvent(@RequestBody ReadyEventDTO eventDTO) {
        Event event = ReadyEventMapper.INSTANCE.toEntity(eventDTO);
        eventService.publishEvent(event);
        return new ResponseEntity<>(event,HttpStatus.OK);
    }

    @PostMapping(value = "/complete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> completeEvent(@RequestBody CompleteEventDTO eventDTO) {
        Event event = CompleteEventMapper.INSTANCE.toEntity(eventDTO);
        eventService.publishEvent(event);
        return new ResponseEntity<>(event,HttpStatus.OK);
    }

    @PostMapping(value = "/inprogress", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> inProgressOrder(@RequestBody InProgressEventDTO eventDTO) {
        Event event = InProgressEventMapper.INSTANCE.toEntity(eventDTO);
        eventService.publishEvent(event);
        return new ResponseEntity<>(event,HttpStatus.OK);
    }
}
