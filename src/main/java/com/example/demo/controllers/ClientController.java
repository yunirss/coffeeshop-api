package com.example.demo.controllers;

import com.example.demo.entity.Client;
import com.example.demo.exceptions.NoSuchEntityException;
import com.example.demo.service.ClientService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> save(@RequestBody Client client) {
        clientService.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> update(@RequestBody Client client) {
        clientService.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Client>> findAll() throws NoSuchEntityException {
        List<Client> clients = clientService.findAll();
        if (clients.isEmpty()) {
            throw new NoSuchEntityException("There is no clients");
        }
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Client> findClientById(@PathVariable Integer id) throws NoSuchEntityException {
        Client client = clientService.findById(id);
        if (client == null) {
            throw new NoSuchEntityException("There is no client with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) {
        clientService.deleteById(id);
        return "Client with Id = " + id + " was deleted";
    }
}
