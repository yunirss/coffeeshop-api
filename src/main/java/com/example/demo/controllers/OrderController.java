package com.example.demo.controllers;

import com.example.demo.entity.Order;
import com.example.demo.exceptions.NoSuchEntityException;
import com.example.demo.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> registerOrder(@RequestBody Order order) {
        orderService.registerOrder(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PostMapping(value = "/{orderId}/cancel", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> cancelOrder(@PathVariable Integer orderId, @RequestParam String reason) {
        orderService.cancelOrder(orderId, reason);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/{orderId}/start", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> setOrderInProgress(@PathVariable int orderId) {
        orderService.setOrderInProgress(orderId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/{orderId}/ready", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> setOrderReady(@PathVariable int orderId) {
        orderService.setOrderReady(orderId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/{orderId}/completed", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> setOrderCompleted(@PathVariable int orderId) {
        orderService.setOrderDelivered(orderId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> findAll() throws NoSuchEntityException {
        List<Order> events = orderService.findAll();
        if (events.isEmpty()) {
            throw new NoSuchEntityException("There is no events");
        }
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> findOrderById(@PathVariable int id) {
        Order order = orderService.findOrder(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}