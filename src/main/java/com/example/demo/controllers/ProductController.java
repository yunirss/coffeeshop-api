package com.example.demo.controllers;

import com.example.demo.entity.Product;
import com.example.demo.exceptions.NoSuchEntityException;
import com.example.demo.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> save(@RequestBody Product product) {
        productService.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> update(@RequestBody Product product) {
        productService.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Product>> findAll() throws NoSuchEntityException, Exception {
        List<Product> products = productService.findAll();
        if (products.isEmpty()) {
            throw new NoSuchEntityException("There is no products");
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> findProductById(@PathVariable Integer id) throws NoSuchEntityException {
        Product product = productService.findById(id);
        if (product == null) {
            throw new NoSuchEntityException("There is no product with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) {
        productService.deleteById(id);
        return "Product with Id = " + id + " was deleted";
    }
}
