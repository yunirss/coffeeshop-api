package com.example.demo.enums;

public enum OrderEventType {
    REGISTERED,
    CANCELLED,
    IN_PROGRESS,
    READY,
    COMPLETED;
}
