package com.example.demo.entity;

import com.example.demo.enums.OrderEventType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "events")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "order_id")
    private int orderId;
    @Column(name = "employee_id")
    private int employeeId;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrderEventType status;
    @Column(name = "details")
    private String details;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.")
    @Column(name = "created_time")
    private LocalDateTime createdTime;
}
