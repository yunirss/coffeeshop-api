package com.example.demo.service;

import com.example.demo.entity.Event;
import com.example.demo.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService implements AbstractService<Event> {
    @Autowired
    private EventRepository eventRepository;

    @Override
    public void save(Event entity) {
        eventRepository.save(entity);
    }

    @Override
    public Event findById(Integer id) {
        return eventRepository.findEventsById(id);
    }

    @Override
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        eventRepository.deleteById(id);
    }

    public List<Event> findEventsByOrderId(Integer id) {
        return eventRepository.findEventsByOrderId(id);
    }

    public void publishEvent(Event event) {
        eventRepository.save(event);
    }
}
