package com.example.demo.service;

import com.example.demo.entity.Client;
import com.example.demo.exceptions.NoSuchEntityException;
import com.example.demo.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService implements AbstractService<Client> {
    @Autowired
    private ClientRepository clientRepository;

    public Client findById(Integer id) {
        Client client = clientRepository.findClientById(id);
        return client;
    }

    public void save(Client client) {
        clientRepository.save(client);
    }

    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    public void deleteById(Integer id) {
        clientRepository.deleteById(id);
    }
}
