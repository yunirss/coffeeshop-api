package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService implements AbstractService<Employee> {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void save(Employee entity) {
        employeeRepository.save(entity);
    }

    @Override
    public Employee findById(Integer id) {
        return employeeRepository.findEmployeeById(id);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        employeeRepository.deleteById(id);
    }
}
