package com.example.demo.service;

import com.example.demo.exceptions.NoSuchEntityException;

import java.util.List;

public interface AbstractService<E> {
    void save(E entity) throws Exception;

    E findById(Integer id) throws Exception;

    List<E> findAll() throws Exception;

    void deleteById(Integer id) throws Exception;
}
