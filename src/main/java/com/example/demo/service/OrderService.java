package com.example.demo.service;

import com.example.demo.entity.Event;
import com.example.demo.entity.Order;
import com.example.demo.enums.OrderEventType;
import com.example.demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private EventService eventService;

    public void registerOrder(Order order) {
        orderRepository.save(order);
        Event event = new Event();
        event.setOrderId(order.getId());
        event.setCreatedTime(LocalDateTime.now());
        event.setStatus(OrderEventType.REGISTERED);
        eventService.save(event);
    }


    public void cancelOrder(Integer id, String reason) {
        Order order = orderRepository.findOrderById(id);
        Event event = new Event();
        event.setOrderId(order.getId());
        event.setStatus(OrderEventType.CANCELLED);
        event.setCreatedTime(LocalDateTime.now());
        eventService.save(event);
    }


    public void setOrderInProgress(Integer id) {
        Order order = orderRepository.findOrderById(id);
        Event event = new Event();
        event.setOrderId(order.getId());
        event.setStatus(OrderEventType.IN_PROGRESS);
        event.setCreatedTime(LocalDateTime.now());
        eventService.save(event);
    }


    public void setOrderReady(Integer id) {
        Order order = orderRepository.findOrderById(id);
        Event event = new Event();
        event.setOrderId(order.getId());
        event.setCreatedTime(LocalDateTime.now());
        event.setStatus(OrderEventType.READY);
        eventService.save(event);
    }

    public void setOrderDelivered(Integer id) {
        Order order = orderRepository.findOrderById(id);
        Event event = new Event();
        event.setOrderId(order.getId());
        event.setCreatedTime(LocalDateTime.now());
        event.setStatus(OrderEventType.COMPLETED);
        eventService.save(event);
    }

    public Order findOrder(int id) {
        return orderRepository.findOrderById(id);
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }
}
