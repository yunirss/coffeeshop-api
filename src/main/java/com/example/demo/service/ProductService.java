package com.example.demo.service;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService implements AbstractService<Product> {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public void save(Product entity) {
        productRepository.save(entity);
    }

    @Override
    public Product findById(Integer id) {
        return productRepository.findProductById(id);
    }

    @Override
    public List<Product> findAll() throws Exception {
        return productRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        productRepository.deleteById(id);
    }
}
